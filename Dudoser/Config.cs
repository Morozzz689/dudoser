﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace Dudoser
{
    public class Config
    {
        public int Amount { get; set; }
        public Request[] Set { get; set; }

        public static Config LoadConfig(string path)
        {
            Config result;

            var content = File.ReadAllText(path);
            result = (Config)JsonSerializer.Deserialize(content, typeof(Config));
            return result;
        }

        private Regex[] _valuePatterns = {
            new Regex(@"#generate_GUID_[0-9a-zA-Z]+#"),
            new Regex(@"#generate_DATENOW#")
        };
        private string[] _setPatterns;

        private void InitPatterns()
        {
            var values = new List<string>() { };
            var set = Set.Select(r => r.Data.ToString()).ToArray();
            foreach (var data in set)
            {
                foreach (var exp in _valuePatterns)
                {
                    foreach (Match match in exp.Matches(data))
                    {
                        values.Add(match.Value);
                    }
                }
            }
            _setPatterns = values.Distinct().ToArray();
        }

        private Dictionary<string, string> InitValues()
        {
            var result = new Dictionary<string, string>(_setPatterns.Length);
            foreach (var pattern in _setPatterns)
            {
                result.Add(pattern, GetValueForPattern(pattern));
            }
            return result;
        }

        public PreparedRequest[] GenerateSetWithValues()
        {
            if (_setPatterns == null)
            {
                InitPatterns();
            }
            var values = InitValues();

            var result = new List<PreparedRequest>(Set.Length);
            foreach (var request in Set)
            {
                string data = request.Data.ToString();
                foreach (var patternValue in values)
                {
                    var pattern = patternValue.Key;
                    var value = patternValue.Value;
                    data = data.Replace($"\"{pattern}\"", value);
                    data = data.Replace(pattern, value.Trim('"'));
                }
                result.Add(new PreparedRequest
                {
                    Method = request.Method,
                    Url = request.Url,
                    Authorization = request.Authorization,
                    ContentType = request.ContentType,
                    Data = Encoding.UTF8.GetBytes(data)
                });
            }

            return result.ToArray();
        }

        public string GetValueForPattern(string pattern)
        {
            string result = string.Empty;
            switch (pattern.Trim('#').Split('_')[1].ToUpper())
            {
                case "GUID":
                    result = $"\"{Guid.NewGuid()}\"";
                    break;
                case "DATENOW":
                    result = $"\"{DateTime.Now:yyyy-MM-ddTHH:mm:ss}\"";
                    break;
            }
            return result;
        }
    }

    public class Request
    {
        public string Method { get; set; }
        public string Url { get; set; }
        public string Authorization { get; set; }
        public string ContentType { get; set; }
        public object Data { get; set; }
    }

    public class PreparedRequest
    {
        public string Method { get; set; }
        public string Url { get; set; }
        public string Authorization { get; set; }
        public string ContentType { get; set; }
        public byte[] Data { get; set; }
    }
}
