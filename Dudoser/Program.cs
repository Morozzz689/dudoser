﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Dudoser
{
    class Program
    {
        static void Main(string[] args)
        {

            var config = Config.LoadConfig("config.json");


            var sendData = new List<PreparedRequest[]>(config.Amount) { };
            for (int i = 0; i < config.Amount; i++)
            {
                sendData.Add(config.GenerateSetWithValues());
            }

            var testMeasures = new List<TimeSpan>(sendData.Count * config.Set.Length);
            Stopwatch totalSw = new Stopwatch();
            totalSw.Start();
            Parallel.ForEach(sendData, (data) =>
            {
                testMeasures.AddRange(SendCommandSet(data));
            });
            totalSw.Stop();

            var min = testMeasures.Min(t => t.TotalMilliseconds);
            var avarage = testMeasures.Sum(t => t.TotalMilliseconds) / testMeasures.Count;
            var max = testMeasures.Max(t => t.TotalMilliseconds);

            Console.WriteLine($"Min send timme {min} milliseconds");
            Console.WriteLine($"Avg send timme {avarage} milliseconds");
            Console.WriteLine($"Max send timme {max} milliseconds");
            Console.WriteLine($"Total send timme {testMeasures.Sum(m => m.TotalMilliseconds) / 1000} seconds");
            Console.WriteLine($"Total timme {totalSw.ElapsedMilliseconds / 1000} seconds");
            Console.ReadLine();
        }

        static void SendCommand(PreparedRequest requestDto)
        {
            var request = WebRequest.Create(requestDto.Url);
            request.Method = requestDto.Method;
            request.Headers.Add("Authorization", requestDto.Authorization);
            request.Timeout = 300000;
            request.ContentType = requestDto.ContentType;
            request.ContentLength = requestDto.Data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(requestDto.Data, 0, requestDto.Data.Length);
            }
            request.GetResponse();
        }

        static List<TimeSpan> SendCommandSet(PreparedRequest[] dataSet)
        {
            List<TimeSpan> timings = new List<TimeSpan>(dataSet.Length) { };
            Stopwatch sw = new Stopwatch();
            sw.Start();
            foreach (var requestDto in dataSet)
            {
                SendCommand(requestDto);
                sw.Stop();
                timings.Add(sw.Elapsed);
                sw.Restart();
            }
            sw.Stop();
            return timings;
        }

    }
}
